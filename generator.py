#!/usr/bin/env python3

import json
import os.path as path
import argparse
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("filename", type=str, help="The required path to the margot json configuration file.")

args = parser.parse_args()
file = open(args.filename,)
locking_features = False

full_margot_file = json.load(file)

file.close()

name = full_margot_file["name"]
version = full_margot_file["version"]
blocks = []
for i in full_margot_file["blocks"]:
    if "features" not in i:
        i["features"] = []
    if "agora" in i:
        to_check = i["agora"]["clustering_parameters"]
        for item in to_check:
            if "adam_lock_features" in item and item["adam_lock_features"].lower()=="true":
                locking_features = True
    block_elem = {}
    block_elem["name"] = i["name"]
    block_elem["metrics"] = []
    block_elem["knobs"] = []
    block_elem["features"] = []
    for j in i["metrics"]:
        metric = {}
        metric["name"] = j["name"]
        metric["type"] = j["type"]
        block_elem["metrics"].append(metric)
    for j in i["knobs"]:
        knob = {}
        knob["name"] = j["name"]
        knob["type"] = j["type"]
        block_elem["knobs"].append(knob)
    for j in i["features"]:
        feature = {}
        feature["name"] = j["name"]
        feature["type"] = j["type"]
        block_elem["features"].append(feature)
    blocks.append(block_elem)

if len(blocks) != 1:
    print("Supposed to work with single block only... Terminating.")
    exit(1)

if path.isfile("./src/adam.cpp"):
    shutil.move("./src/adam.cpp", "./src/adam_old.cpp")

cpp_file = open("./src/adam.cpp", "w")
cpp_file.writelines([
    "#include <margot/margot.hpp>\n",
    "#include <chrono>\n",
    "#include <thread>\n",
    "#include <list>\n",
    "#include <string>\n",
    "#include \"mqtt_client.h\"\n",
    "#include <stdlib.h>\n"
])

cpp_file.writelines([
    "int main(int argc, char** argv)\n",
    "{\n",
    "    if(argc <= 1)\n",
    "    {\n",
    "        printf(\"Too few arguments, needed 1 (seed). Aborting...\\n\");\n",
    "        exit(0);\n",
    "    }\n",
    "    margot::init();\n",
    "    std::list<std::string> knobs;\n",
    "    std::list<std::string> metrics;\n",
    "    std::list<std::string> features;\n",
])

for k in blocks[0]["knobs"]:
    cpp_file.write("    knobs.push_back(\"" + k["name"] + "\");\n")

for m in blocks[0]["metrics"]:
    cpp_file.write("    metrics.push_back(\"" + m["name"] + "\");\n")

for f in blocks[0]["features"]:
    cpp_file.write("    features.push_back(\"" + f["name"] + "\");\n")

cpp_file.writelines([
    "    std::unordered_map<std::string, std::list<std::string>> structure(\n",
    "        {{\"knobs\",knobs}, {\"metrics\", metrics}, {\"features\", features}});\n",
    "    Adam::init(\"" + name + "^" + version + "^" + blocks[0]["name"] + "\", atol(argv[1]), structure);\n"
])

for k in blocks[0]["knobs"]:
    cpp_file.write("    "+ k["type"] + " " + k["name"] + " = " + ("\"\"" if k["type"] == "string" else "0") + ";\n")

for f in blocks[0]["features"]:
    cpp_file.write("    "+ f["type"] + " " + f["name"] + " = 0;\n")

cpp_file.writelines([
    "    printf(\"Waiting for configuration...\");\n",
    "    margot::" + blocks[0]["name"] + "::context().manager.wait_for_knowledge(5);\n",
    "    printf(\" Config ok, proceeding...\\n\");\n",
    "",
    "    while(!Adam::get_instance()->execution_to_be_finished())\n",
    "    {\n"
])
if len(blocks[0]["features"]):
    if locking_features:
        cpp_file.writelines([
        "        printf(\"Getting features...\");\n",
        "        auto read_features = Adam::get_instance()->get_features_blocking();\n",
        "        printf(\" Features ok.\\n\");\n"
        ])
    else:
        cpp_file.writelines([
        "        printf(\"Getting features...\");\n",
        "        auto read_features = Adam::get_instance()->get_features();\n",
        "        printf(\" Features ok.\\n\");\n"
        ])

    for f in blocks[0]["features"]:
        if f["type"] in [
            "int",
            "unsigned",
            "short",
            "long",
            "long long"]:
            cpp_file.write("        "+f["name"] + " = std::stoi(read_features.front());\n")
        elif f["type"] in [
            "char"]:
            cpp_file.write("        "+f["name"] + " = read_features.front().c_str()[0];\n")
        elif f["type"] in [
            "float"]:
            cpp_file.write("        "+f["name"] + " = std::stof(read_features.front());\n")
        elif f["type"] in [
            "double"]:
            cpp_file.write("        "+f["name"] + " = std::stod(read_features.front());\n")

        cpp_file.write("        read_features.pop_front();\n")

features_names = ", ".join([x["name"] for x in blocks[0]["features"]])
if features_names != "":
    features_names += ", "
knobs_names = ", ".join([x["name"] for x in blocks[0]["knobs"]])

cpp_file.writelines([
    "        if(margot::" + blocks[0]["name"] + "::update(" + features_names + knobs_names + "))\n",
    "        {\n"
])

cpp_file.writelines([
    "            margot::" + blocks[0]["name"] + "::context().manager.configuration_applied();\n",
    "        }\n",
    "        std::list<std::string> configuration;\n"
])

for k in blocks[0]["knobs"]:
    cpp_file.write("        configuration.push_back(std::to_string(" + k["name"] + "));\n")
cpp_file.writelines([
    "        printf(\"Sending knob configuration.\\n\");\n",
    "        Adam::get_instance()->set_configuration(configuration);\n",
    "        margot::" + blocks[0]["name"] + "::start_monitors();\n",
    "        margot::" + blocks[0]["name"] + "::stop_monitors();\n",
    "        printf(\"Waiting for the results of the execution...\");\n",
    "        std::list<std::string> metrics_list = Adam::get_instance()->get_metrics();\n",
    "        printf(\" Metrics ok.\\n\");\n",
    "        std::vector<std::string> metrics_vector(metrics_list.begin(), metrics_list.end());\n"
])


metrics_params = []
index = 0
for m in blocks[0]["metrics"]:
    if m["type"] in [
        "int",
        "unsigned",
        "short",
        "long",
        "long long"]:
        metrics_params.append("std::stoi(metrics_vector[" + str(index) + "])")
    elif m["type"] in [
        "char"]:
        metrics_params.append("metrics_vector[" + str(index) + "].c_str()[0]")
    elif m["type"] in [
        "float"]:
        metrics_params.append("std::stof(metrics_vector[" + str(index) + "])")
    elif m["type"] in [
        "double"]:
        metrics_params.append("std::stod(metrics_vector[" + str(index) + "])")

    index += 1

cpp_file.writelines([
    "        margot::" + blocks[0]["name"] + "::push_custom_monitor_values(" + ", ".join(metrics_params) + ");\n",
    "        margot::" + blocks[0]["name"] + "::log();\n",
    "    }\n",
    "    Adam::delete_instance();\n",
    "}"
])

cpp_file.close()

print("-- > Adam File generated!")

shutil.move("./CMakeLists_original.txt", "./CMakeLists.txt")

with open("CMakeLists.txt", "r+") as cmakefile:
    content = cmakefile.read()

content = content.replace("<path_of_margot_config_file>",args.filename)

shutil.move("./CMakeLists.txt", "./CMakeLists_original.txt")
with open("CMakeLists.txt", "w+") as file:
    file.write(content)


print("-- > CMakeFile adjusted!")
print("-- > Ready to compile!")