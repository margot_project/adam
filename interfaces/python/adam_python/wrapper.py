from functools import wraps
from pydoc import locate
import json

def tune(tuner, knobs, metrics, features = []):
    def outer(func):
        @wraps(func)
        def inner(*args, **kwargs):
            tuner.init()
            out_features = {}
            for f in features:
                out_features[f["name"]] = str(f["function"]())
            tuner.set_input_features(out_features)
            in_knobs = tuner.get_configuation()
            for k in in_knobs:
                entry = next(i for i in knobs if i["name"] == k)
                in_knobs[k] = locate(entry["type"])(in_knobs[k])
            ret_val = func(*args, **kwargs, **in_knobs)
            out_metrics = {}
            for m in metrics:
                out_metrics[m["name"]] = str(m["function"](ret_val))
            tuner.set_metrics(out_metrics)
            return ret_val
        return inner
    return outer

def tune_file(tuner, file_name):
    with open(file_name,) as file:
        json_file = json.load(file)
        return tune(tuner, json_file["knobs"],json_file["metrics"],json_file["features"])

def knob(name, type):
    def outer(func):
        @wraps(func)
        def inner(*args, **kwargs):
            if "adam_knobs" in kwargs:
                kwargs["adam_knobs"].append({"name": name, "type": type})
            else:
                kwargs["adam_knobs"] = [{"name": name, "type": type}]
            return func(args, kwargs)
        return inner
    return outer

def metric(name, type, function):
    def outer(func):
        @wraps(func)
        def inner(*args, **kwargs):
            if "adam_metrics" in kwargs:
                kwargs["adam_metrics"].append({"name": name, "type": type, "function": function})
            else:
                kwargs["adam_metrics"] = [{"name": name, "type": type, "function": function}]
            return func(args, kwargs)
        return inner
    return outer

def feature(name, type, function):
    def outer(func):
        @wraps(func)
        def inner(*args, **kwargs):
            if "adam_features" in kwargs:
                kwargs["adam_features"].append({"name": name, "type": type, "function": function})
            else:
                kwargs["adam_features"] = [{"name": name, "type": type, "function": function}]
            return func(args, kwargs)
        return inner
    return outer

def tune_wrapped(tuner):
    def outer(func):
        @wraps(func)
        def inner(*args, **kwargs):
            if "adam_knobs" in kwargs:
                knobs = kwargs["adam_knobs"]
                del kwargs["adam_knobs"]
            else:
                knobs = []
            if "adam_metrics" in kwargs:
                metrics = kwargs["adam_metrics"]
                del kwargs["adam_metrics"]
            else:
                metrics = []
            if "adam_features" in kwargs:
                features = kwargs["adam_features"]
                del kwargs["adam_features"]
            else:
                features = []
            return tune(tuner, knobs, metrics, features)
        return inner
    return outer