#include "MQTTClient.h"
#include <cstdio>
#include <cstring>
#include <string>

#include "mqtt_callbacks.h"
#include "mqtt_client.h"

volatile MQTTClient_deliveryToken deliveredtoken;

void mqtt_callbacks::on_deliver(void *context, MQTTClient_deliveryToken dt)
{
    printf("Message with token value %d delivered\n", dt);
    deliveredtoken = dt;
}

int mqtt_callbacks::on_message(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{   
    int i;
    char* payloadptr;
    payloadptr = (char*) message->payload;
    std::string payloadstr = std::string(payloadptr);
    payloadstr.resize(message->payloadlen);
    if(strstr(topicName,"IFfetch"))
        Adam::get_instance()->handle_fetch(payloadstr);
    else if(strstr(topicName,"metrics"))
        Adam::get_instance()->handle_metrics(payloadstr);
    else if(strstr(topicName,"stop"))
        Adam::get_instance()->handle_stop(payloadstr);
    else if(strstr(topicName, "welcome"))
        Adam::get_instance()->handle_welcome(payloadstr);
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}
void mqtt_callbacks::on_disconnect(void *context, char *cause)
{
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}