#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unordered_map>
#include <list>
#include <sstream>
#include <iostream>
#include <condition_variable>
#include "MQTTClient.h"
#include "mqtt_client.h"
#include "mqtt_callbacks.h"
#include <nlohmann/json.hpp>
#define ADDRESS     "tcp://localhost:1883"
#define CLIENTID    "ExampleClientSub"
#define TIMEOUT     10000L

using json = nlohmann::json;

void Adam::handle_fetch(std::string message)
{
    json deserialized = json::parse(message);    
    auto features = app_structure.find("features");
    std::list<std::string> output = {};
    for(auto list_iter = features->second.begin(); list_iter != features->second.end(); ++list_iter)
    {
        std::string value = deserialized[*list_iter];
        output.push_back(value);
    }
    std::lock_guard<std::mutex> lock(data_lock);
    available_features = true;
    last_received_features = output;
    features_received.notify_all();
}

void Adam::handle_metrics(std::string message)
{
    json deserialized = json::parse(message);    
    auto metrics = app_structure.find("metrics");
    std::list<std::string> output = {};
    for(auto list_iter = metrics->second.begin(); list_iter != metrics->second.end(); ++list_iter)
    {
        std::string value = deserialized[*list_iter];
        output.push_back(value);
    }
    std::lock_guard<std::mutex> lock(data_lock);
    available_metrics = true;
    last_received_metrics = output;
    metrics_received.notify_all();
}

void Adam::handle_stop(std::string message)
{
    std::lock_guard<std::mutex> lock(data_lock);
    stop = true;
    MQTTClient_disconnect(client, 10000);
    //MQTTClient_destroy(&client);
    metrics_received.notify_all();
}

void Adam::handle_welcome(std::string message)
{
    std::lock_guard<std::mutex> lock(data_lock);
    started = true;
    welcome_received.notify_all();
}

Adam::Adam(std::string app_name, long seed, std::unordered_map<std::string, std::list<std::string>> structure)
{
    srand(time(NULL));
    stop = false;
    started = false;
    available_metrics = false;
    available_features = false;
    app = app_name;
    app_structure = structure;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int rc;
    int ch;
    char id[20];
    snprintf(id, 20, "%d", rand());
    MQTTClient_create(&client, ADDRESS, id,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    MQTTClient_setCallbacks(client, NULL, mqtt_callbacks::on_disconnect, mqtt_callbacks::on_message, mqtt_callbacks::on_deliver);
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        exit(EXIT_FAILURE);
    }
    //random_seed = rand() % 256;
    random_seed = seed;

    MQTTClient_subscribe(client, std::string("adam/" + std::to_string(random_seed) + "/welcome").c_str(), 0);
    MQTTClient_subscribe(client, std::string("adam/" + std::to_string(random_seed) + "/IFfetch").c_str(), 0);
    MQTTClient_subscribe(client, std::string("adam/" + std::to_string(random_seed) + "/metrics").c_str(), 0);
    MQTTClient_subscribe(client, std::string("adam/" + std::to_string(random_seed) + "/stop").c_str(), 0);
}

void Adam::set_configuration(std::list<std::string> values)
{
    json json_message;

    auto knobs = app_structure.find("knobs");

    auto it = values.begin();

    for(auto list_iter = knobs->second.begin(); list_iter != knobs->second.end(); ++list_iter)
        json_message[*list_iter] = *(it++);

    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    std::string message = json_message.dump();
    pubmsg.payload = (void*) message.c_str();
    pubmsg.payloadlen = message.size();
    pubmsg.qos = 0;
    pubmsg.retained = 0;
    MQTTClient_publishMessage(client, std::string("adam/" + std::to_string(random_seed) + "/config").c_str(), &pubmsg, &token);
}

std::list<std::string> Adam::get_features()
{
    std::unique_lock<std::mutex> lk(data_lock);
    features_received.wait(lk, [this]{return !last_received_features.empty();});
    return last_received_features;
}

std::list<std::string> Adam::get_features_blocking()
{
    std::unique_lock<std::mutex> lk(data_lock);
    features_received.wait(lk, [this]{return available_features || stop;});
    if(stop)
        exit(0);
    available_features = false;
    return last_received_features;
}

std::list<std::string> Adam::get_metrics()
{
    std::unique_lock<std::mutex> lk(data_lock);
    metrics_received.wait(lk, [this]{return available_metrics || stop;});
    if(stop)
        exit(0);
    available_metrics = false;
    return last_received_metrics;
}

void Adam::init(std::string block, long seed, std::unordered_map<std::string, std::list<std::string>> structure)
{
    assert(!instance);
    instance = new Adam(block, seed, structure);
    return Adam::get_instance()->link();
}

void Adam::link()
{
    printf("Started connection for app %s, tag %ld. Sending welcome message...\n", app.c_str(), random_seed);
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    std::string message = "Here I am";
    pubmsg.payload = (void*) message.c_str();
    pubmsg.payloadlen = message.size();
    pubmsg.qos = 0;
    pubmsg.retained = 1;
    MQTTClient_publishMessage(client, std::string("adam/" + std::to_string(random_seed) + "/welcome").c_str(), &pubmsg, &token);
}

Adam* Adam::get_instance()
{
    assert(instance);
    return instance;
}

void Adam::delete_instance()
{
    assert(instance);
    delete instance;
}

bool Adam::execution_to_be_finished()
{
    std::unique_lock<std::mutex> lk(data_lock);
    return stop;
}