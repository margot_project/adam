import sys
from setuptools import setup, find_packages

if sys.version_info.major < 3 or (
        sys.version_info.major == 3 and sys.version_info.minor < 6
):
    sys.exit("Python 3.6 or new is required")

VERSION = "0.1"

with open("requirements.txt") as reqs:
    requirements = [line.strip().split("==")[0] for line in reqs.readlines()]

setup(
    name="adam_python",
    version=VERSION,
    description="Adam interface for using mARGOt with Python apps",
    author="Roberto Rocco",
    author_email="roberto.rocco@polimi.it",
    packages=find_packages(),
    install_requires=requirements,
    entry_points={}
)
