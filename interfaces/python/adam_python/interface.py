import json
from urllib import request
import paho.mqtt.client as mqtt
from snowflake import SnowflakeGenerator
from time import sleep
import threading


class Tuner:
    def wait_for_completion(self):
        self.message_cond.acquire()
        while not self.message_sent:
            self.message_cond.wait()
        self.message_sent = False
        self.message_cond.release()

    def set_configuration(self, msg):
        self.config_cond.acquire()
        try:
            self.last_received_configuration = json.loads(msg.payload.decode("utf-8"))
            self.configuration_fetched = False
            self.config_cond.notify()
        finally:
            self.config_cond.release()

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe("adam/" + str(self.seed) + "/config")
        client.subscribe("adam/" + str(self.seed) + "/welcome")
        if self.json_location:
            with open(self.json_location, "r") as f:
                configs = json.load(f)
            client.publish("adam/" +str(self.seed) + "/request", json.dumps(configs))
    
    def on_connect_request(self, client, userdata, flags, rc):
        temp_id = next(SnowflakeGenerator(69))
        client.subscribe("brian/" + str(temp_id) + "/app_id")
        with open(self.json_location, "r") as f:
            configs = json.load(f)
        client.publish("brian/" +str(temp_id) + "/get_id", json.dumps(configs))

    def wait_for_id(self, client, userdata, msg):
        self.seed = msg.payload.decode("utf-8")
        self.json_location = None
        self.client.loop_stop()
        self.init()

    def on_publish(self, client, userdata, mid):
        self.message_cond.acquire()
        self.message_sent = True
        self.message_cond.notify()
        self.message_cond.release()

    def wait_started(self):
        self.welcome_cond.acquire()
        while not self.welcome_received:
            self.welcome_cond.wait()
        self.welcome_cond.release()


    def message_dispatcher(self, client, userdata, msg):
        if "welcome" in msg.topic:
            self.welcome_cond.acquire()
            self.welcome_received = True
            self.welcome_cond.notify()
            self.welcome_cond.release()
            if msg.retain:
                client.publish(msg.topic, payload=None, retain=True)
        else:
            self.set_configuration(msg)

### API ###

    def __init__(self, json_loc = None, broker_ip = "127.0.0.1", seed = None):
        assert not (seed == "request" and json_loc == None), "Wrong Adam initialisation! Cannot run in request mode without a json configuration file."
        self.last_received_configuration = dict()
        self.configuration_fetched = True
        self.config_cond = threading.Condition()
        self.welcome_received = False
        self.welcome_cond = threading.Condition()
        self.message_sent = False
        self.message_cond = threading.Condition()
        if not seed:
            self.seed = next(SnowflakeGenerator(69))
            print(self.seed)
        else:
            self.seed = seed
        self.json_location = json_loc
        self.initialized = False
        self.broker_ip = broker_ip
        #self.init()

    def init(self):
        if not self.initialized:
            self.client = mqtt.Client()
            if self.seed == "request":
                self.client.on_connect = self.on_connect_request
                self.client.on_message = self.wait_for_id
            else:
                self.initialized = True
                self.client.on_connect = self.on_connect
                self.client.on_message = self.message_dispatcher
                self.client.on_publish = self.on_publish
                self.client.will_set("adam/" + str(self.seed) + "/stop", payload="Bye bye", qos=0)
            self.client.connect(self.broker_ip, 1883, 60)
            self.client.loop_start()

    def __del__(self):
        #self.wait_started()
        #self.client.publish("adam/" + str(self.seed) + "/stop", "Bye bye")
        #self.wait_for_completion()
        self.client.loop_stop()

    def get_configuation(self):
        self.config_cond.acquire()
        while self.configuration_fetched:
            self.config_cond.wait()
        temp = self.last_received_configuration
        self.configuration_fetched = True
        self.config_cond.release()
        return temp

    def set_input_features(self, data):
        self.wait_started()
        self.client.publish("adam/" + str(self.seed) + "/IFfetch", json.dumps(data))
        self.wait_for_completion()

    def set_metrics(self, data):
        self.wait_started()
        self.client.publish("adam/" + str(self.seed) + "/metrics", json.dumps(data))
        self.wait_for_completion()
