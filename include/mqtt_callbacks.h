#ifndef MQTT_CALLBACKS_H
#define MQTT_CALLBACKS_H

#include "MQTTClient.h"

namespace mqtt_callbacks
{
    void on_deliver(void*, MQTTClient_deliveryToken);
    int on_message(void*, char*, int, MQTTClient_message*);
    void on_disconnect(void*, char*);
}

#endif