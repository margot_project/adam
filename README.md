# Adam

Adam (ADApter for Margot) is a component able to make non_cpp applications work with mARGOt. It defines a MQTT interface that can be implemented in other languages to fully support all the mARGOt functionalities.

## Interface definition

The communication between Adam and the application is performed over MQTT and uses the following topics:

* adam/[adam_id]/welcome: it is used by Adam to notify its readiness to the application. The content of the message is irrelevant
* adam/[adam_id]/IFfetch: it is used by the application to send the value of the input features fetched. The informations sent are in the form of a json dictionary, with the names of the features as key and a string representation of the values as values.
* adam/[adam_id]/config: it is used by Adam to share the execution configuration to the application. The message will be delivered in the form of a json dictionary, with knob names as keys and a string representation of the values as values.
* adam/[adam_id]/metrics: it is used by the application to send the value of the metrics to Adam. The structure is analogous to the structures sent in the messages above.
* adam/[adam_id]/stop: it is used by the application to notify Adam about the termination of the execution. The content of the message is irrelevant.

All these topics contain [adam_id], which is a number used to identify different Adam instances. The number must be passed as parameter when executing Adam. An example of a complete topic is adam/212133391/welcome/

## Available interfaces

Interfaces compatible with the above definition will be provided inside the interfaces folder

## Build instructions

Change the CMakeLists.txt from line 11 to 16 to point to the correct location of the modules and files (in particular, line 14 should contain the path of the _margot.json_ configuration file). After that, run a classic cmake build:

```
mkdir build
cd build
cmake ..
make -j
```

## Execution instructions

```
./build/adam adam_id
```

### Acknowledgment

This work has been supported by European Commission under the following grants:

* EVEREST No. 957269 (dEsign enVironmEnt foR Extreme-Scale big data analyTics on heterogeneous platforms)
