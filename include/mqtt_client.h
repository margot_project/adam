#ifndef MQTT_CLIENT_H
#define MQTT_CLIENT_H

#define ADDRESS     "tcp://localhost:1883"
#define CLIENTID    "ExampleClientSub"
#define TIMEOUT     10000L

#include <unordered_map>
#include <list>
#include <string>
#include <condition_variable>
#include "MQTTClient.h"

class Adam
{
    private:
        MQTTClient client;
        bool stop;
        std::unordered_map<std::string, std::list<std::string>> app_structure;
        std::string app;
        std::list<std::string> last_received_metrics;
        std::list<std::string> last_received_features;
        bool available_metrics;
        bool available_features;
        std::condition_variable metrics_received;
        std::mutex data_lock;
        std::condition_variable features_received;
        inline static Adam* instance;
        bool started;
        std::condition_variable welcome_received;
        long random_seed;
        void link();
    
        Adam(std::string, long, std::unordered_map<std::string, std::list<std::string>>);
        Adam(const Adam&) = delete;
        void operator= (const Adam&) = delete;
    public:

        bool execution_to_be_finished();
        void handle_fetch(std::string);
        void handle_metrics(std::string);
        void handle_stop(std::string);
        void handle_welcome(std::string);
        static void init(std::string, long, std::unordered_map<std::string, std::list<std::string>>);
        static Adam* get_instance();
        static void delete_instance();
        void set_configuration(std::list<std::string>);
        std::list<std::string> get_features();
        std::list<std::string> get_features_blocking();
        std::list<std::string> get_metrics();
};


#endif